﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MigrationJob
{
    class Program
    {
        static void Main(string[] args)
        {

            DirectoryInfo di = new DirectoryInfo(@"D:\DataMigration\MigrationJob\migration");//here  give file path where excel sheet available
            FileInfo[] files = di.GetFiles("*");
            foreach (var file in files)
            {
                DataTable dataTable = null;
                if (file.Extension.ToLower() == ".csv")
                {
                    dataTable = GetDataTableFromCsv(file.FullName, true);
                }
                if (file.Extension.ToLower() == ".xlsx" || file.Extension.ToLower() == ".xls")
                {
                    dataTable = exceldata(file.FullName, true);
                }
                //DataTable dataTable = GetDataTableFromCsv(file.FullName, true);
                // DataTable dataTable = exceldata(file.FullName, true);
                //dataTable.Columns.Remove("columnName");
                dataTable.Columns.Remove("Lead Status");
                dataTable.Columns.Remove("Verified");
                dataTable.Columns.Remove("Project Count");
                dataTable.Columns.Remove("Min Budget");
                dataTable.Columns.Remove("Min Possession");
                dataTable.Columns.Remove("First-Medium");
                dataTable.Columns.Remove("Last-Medium");
                dataTable.Columns.Remove("Lead Owner Email ID");
                dataTable.Columns.Remove("Created At(System Date)");
                dataTable.Columns.Remove("First contacted on");
                dataTable.Columns.Remove("Pushed On");
                dataTable.Columns.Remove("Pushed by");
                dataTable.Columns.Remove("Reassigned By");
                dataTable.Columns.Remove("Reassigned On");
                dataTable.Columns.Remove("Last Sales Contact Attempted On");
                dataTable.Columns.Remove("Total Incoming Answered Calls");
                dataTable.Columns.Remove("Total Incoming Not Answered Calls");
                dataTable.Columns.Remove("Total Outgoing Answered Calls");
                dataTable.Columns.Remove("Total Outgoing Not Answered Calls");
                dataTable.Columns.Remove("Dropoff On");
                dataTable.Columns.Remove("Dropoff By");
                dataTable.Columns.Remove("System Tags");
                dataTable.Columns.Remove("Manual Tags");
                dataTable.Columns.Remove("Secondary Sales");
                dataTable.Columns.Remove("Pulled Date");
                dataTable.Columns.Remove("Pulled by");
                dataTable.Columns.Remove("Pulled from");
                dataTable.Columns.Remove("GCLID");
                dataTable.Columns.Remove("Company Name");
                dataTable.Columns.Remove("Designation");
                dataTable.Columns.Remove("Incomes");
                dataTable.Columns.Remove("Date Of Birth");
                dataTable.Columns.Remove("Industry");
                dataTable.Columns.Remove("Attended By Sales Id");
                dataTable.Columns.Remove("Woodsville");
                dataTable.Columns.Remove("L-Axis");
                dataTable.Columns.Remove("Puneville ");  //you need to crosscheck this coloum name in your excel sheet 

                dataTable.Columns["Lead-Id"].ColumnName = "LegacyId";
                dataTable.Columns["Salutation"].ColumnName = "Title";
                dataTable.Columns["First Name"].ColumnName = "FirstName";
                dataTable.Columns["Lead Stage"].ColumnName = "Lead_Stage";
                dataTable.Columns["Phone"].ColumnName = "MobileNo";
                dataTable.Columns["Phone Country"].ColumnName = "Country";
                dataTable.Columns["Secondary Phones"].ColumnName = "AlternateNo";
                dataTable.Columns["Email"].ColumnName = "Email";
                dataTable.Columns["Secondary Emails"].ColumnName = "SecondaryEmails";
                dataTable.Columns["Project"].ColumnName = "InteractionComment";
                dataTable.Columns["Location"].ColumnName = "PreferredLocation";
                dataTable.Columns["Property Type"].ColumnName = "RequirementType";
                dataTable.Columns["Max Budget"].ColumnName = "Budget";
                dataTable.Columns["Max Possession"].ColumnName = "Possession";
                dataTable.Columns["Bedroom Preference"].ColumnName = "BHK";
                dataTable.Columns["Purpose"].ColumnName = "Purpose";
                dataTable.Columns["First-Source Of Enquiry"].ColumnName = "Source";
                dataTable.Columns["First-Campaign"].ColumnName = "Campaign";
                dataTable.Columns["First-Sub Source"].ColumnName = "First_SubSource";
                dataTable.Columns["Last-Source Of Enquiry"].ColumnName = "LastSource";
                dataTable.Columns["Last-Campaign"].ColumnName = "LastCampaign";
                dataTable.Columns["Last-Sub Source"].ColumnName = "LastSubSource";
                dataTable.Columns["Re-engaged Count"].ColumnName = "Counter";
                dataTable.Columns["Attended By"].ColumnName = "AttendedBy";
                dataTable.Columns["Received On"].ColumnName = "CreatedOn";
                dataTable.Columns["Last Contacted On"].ColumnName = "Last_Contacted_On";
                dataTable.Columns["Last Sales Contacted On"].ColumnName = "LastSalesContactedOn";
                dataTable.Columns["Site visit Status"].ColumnName = "Site_visit_Status";
                dataTable.Columns["Next Site visit Date"].ColumnName = "PreferredDate";
                dataTable.Columns["Third Last Note"].ColumnName = "Third_Last_Note";
                dataTable.Columns["Second Last Note"].ColumnName = "Second_Last_Note";
                dataTable.Columns["Last note"].ColumnName = "Last_note";
                dataTable.Columns["Last Call Note"].ColumnName = "LastCallNote";
                dataTable.Columns["Dropoff Reason"].ColumnName = "StatusChangeReasone";
                dataTable.Columns["Reengaged Dates With Sources"].ColumnName = "ReengagedDatesWithSourceDetails";
                dataTable.Columns["Attended By Team"].ColumnName = "AttendedByTeam";
                dataTable.Columns["Followup Status"].ColumnName = "Followup_Status";
                dataTable.Columns["Next Followup Date"].ColumnName = "FollowupDate";
                dataTable.Columns["Last Name"].ColumnName = "LastName";

                dataTable.Columns.Add("Attented_ById", typeof(System.Int32));
                dataTable.Columns.Add("PreferredProject", typeof(System.String));

                string sqlWhere = "LegacyId Is Not Null";         //     select where LegacyId is not null 
                DataTable newDataTable = dataTable.Select(sqlWhere).CopyToDataTable(); //copy main table in new table where legacyid is null
                DataTable dtUpdated = UpdateDAtaTable(newDataTable);
                InsertintoTable(dtUpdated);
            }
        }

        private static DataTable UpdateDAtaTable(DataTable dataTable)
        {
            foreach (DataRow row in dataTable.Rows)
            {
                int id = GetIDbyName(Convert.ToString(row["AttendedBy"])); //AttendedBy 
                row["Attented_ById"] = id;
                if (Convert.ToString(row["Country"]).ToLower() == "india" && Convert.ToString(row["MobileNo"]).Length >= 10)
                {
                    row["MobileNo"] = Convert.ToString(row["MobileNo"]) != "" ? Convert.ToString(row["MobileNo"]).Substring(Convert.ToString(row["MobileNo"]).Length - 10) : "";
                }
                if (row["Email"] == null || Convert.ToString(row["Email"]) == "")
                {
                    row["Email"] = row["MobileNo"] + "@trurealty.in";
                }
                row["PreferredProject"] = Convert.ToString(row["InteractionComment"]) != "" ? Convert.ToString(row["InteractionComment"]).Split(',')[0] : "";
            }
            return dataTable;
        }


        private static int GetIDbyName(string AttendedBy)
        {
            int val = 0;
            const string Connectionstring = "Server=tcp:trurealty.database.windows.net,1433;Initial Catalog=Tru_DB;Persist Security Info=False;User ID=sujaykalele;Password=tru@1234;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30";
            using (SqlConnection con = new SqlConnection(Connectionstring))
            {
                using (SqlCommand cmd = new SqlCommand("GetEmployeeIdBaseOnName", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    if (AttendedBy != null || AttendedBy != "")
                    {
                        cmd.Parameters.AddWithValue("@AttendedBy", AttendedBy);

                        con.Open();
                        //val = (int)cmd.ExecuteScalar();
                        object result = cmd.ExecuteScalar();
                        result = (result == DBNull.Value) ? null : result;
                        val = Convert.ToInt32(result);
                    }
                    else
                    { val = 0; }
                }
            }
            return val;
        }

        private static void InsertintoTable(DataTable dataTable)
        {
            using (var bulkCopy = new SqlBulkCopy("Data Source=tcp:trurealty.database.windows.net,1433;Initial Catalog=Tru_DB;User ID=sujaykalele;Password=tru@1234", SqlBulkCopyOptions.KeepIdentity))
            {
                // my DataTable column names match my SQL Column names, so I simply made this loop. However if your column names don't match, just pass in which datatable name matches the SQL column name in Column Mappings
                foreach (DataColumn col in dataTable.Columns)
                {
                    bulkCopy.ColumnMappings.Add(col.ColumnName, col.ColumnName);
                }

                bulkCopy.BulkCopyTimeout = 600;
                bulkCopy.DestinationTableName = "AllLeads_MigrationTest1";
                bulkCopy.WriteToServer(dataTable);
            }
        }

        static DataTable GetDataTableFromCsv(string path, bool isFirstRowHeader)
        {
            string header = isFirstRowHeader ? "Yes" : "No";

            string pathOnly = Path.GetDirectoryName(path);
            string fileName = Path.GetFileName(path);

            string sql = @"SELECT * FROM [" + fileName + "]";

            using (OleDbConnection connection = new OleDbConnection(
                      @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + pathOnly +
                      ";Extended Properties=\"Text;HDR=" + header + "\""))
            using (OleDbCommand command = new OleDbCommand(sql, connection))
            using (OleDbDataAdapter adapter = new OleDbDataAdapter(command))
            {
                DataTable dataTable = new DataTable();
                dataTable.Locale = CultureInfo.CurrentCulture;
                adapter.Fill(dataTable);
                return dataTable;
            }
        }

        public static DataTable exceldata(string filePath, bool isFirstRowHeader)
        {
            DataTable dtexcel = new DataTable();
            bool hasHeaders = isFirstRowHeader;
            string HDR = hasHeaders ? "Yes" : "No";
            string strConn;
            if (filePath.Substring(filePath.LastIndexOf('.')).ToLower() == ".xlsx")
                strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + filePath + ";Extended Properties=\"Excel 12.0;HDR=" + HDR + ";IMEX=0\"";
            else
                strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + filePath + ";Extended Properties=\"Excel 8.0;HDR=" + HDR + ";IMEX=0\"";
            OleDbConnection conn = new OleDbConnection(strConn);
            conn.Open();
            DataTable schemaTable = conn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, new object[] { null, null, null, "TABLE" });
            //Looping Total Sheet of Xl File
            /*foreach (DataRow schemaRow in schemaTable.Rows)
            {
            }*/
            //Looping a first Sheet of Xl File
            DataRow schemaRow = schemaTable.Rows[0];
            string sheet = schemaRow["TABLE_NAME"].ToString();
            if (!sheet.EndsWith("_"))
            {
                string query = "SELECT  * FROM [leadExport28and29Jan2019$]"; //here add your excel sheet name which you want to insert
                OleDbDataAdapter daexcel = new OleDbDataAdapter(query, conn);
                dtexcel.Locale = CultureInfo.CurrentCulture;
                daexcel.Fill(dtexcel);
            }

            conn.Close();
            return dtexcel;

        }

    }
}
